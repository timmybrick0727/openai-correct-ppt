import json
import openai
import time
import re
import yaml
import tkinter as tk
from tkinter import filedialog, Text, Frame, Label
from pptx import Presentation
from tkinter import font
from pptx import Presentation

base_prompt = "以下JSON為一個pptx的內容，參考離PPT頁面左邊、上邊距離，對「內容」進行（1.拼寫檢查 2.語法檢查 3.標點符號必須為全形 4.檢查所有頁面所有文字及表格）；請用繁體中文條列需要修正之處「哪一頁：建議是什麼？修改前、修改後成什麼樣子？」"

######################################################################## OpenAI基本設定
openai.api_type = "azure"
openai.api_base = "https://eademogpt4.openai.azure.com/"
openai.api_version = "2023-05-15"
openai.api_key = "fefb546daf384d20a82c5e52e41a40f3"
engine = "EA_GPT4_32K"
temperature = 0.5
max_tokens = 4000
frequency_penalty = 0.5
timeout = 10
sleep_time = 2


######################################################################## OpenAI相關function
def clean_text(text):
    text = re.sub("<.*?>", "", text)
    text = re.sub("&nbsp;", " ", text)
    text = re.sub("&amp;", "&", text)
    text = re.sub("&gt;", ">", text)
    text = re.sub("&lt;", "<", text)
    return text


def get_answer(messages):
    result = clean_text(
        openai.ChatCompletion.create(
            engine=engine,
            messages=messages,
            temperature=temperature,
            max_tokens=max_tokens,
            top_p=0.95,
            frequency_penalty=frequency_penalty,
            presence_penalty=0,
            stop=None,
        )["choices"][0]["message"]["content"]
    )
    time.sleep(sleep_time)
    return result


def display_answer(data):
    # 翻譯「定義」
    content = get_answer(
        [
            {
                "role": "user",
                "content": f"{base_prompt}\n\n{data}",
                # "content": f"鲁迅为什么暴打周树人", // 這個問題會讓 GPT-3 亂回答，用來測試是否為 GPT-4（魯迅 == 周樹人）
            }
        ]
    )
    text_widgets[1].delete(1.0, tk.END)
    text_widgets[1].insert(tk.END, content)


######################################################################## PPT相關function
def remove_spaces_between_chinese_and_english(text):
    # 1. English after Chinese
    text = re.sub(r"([\u4e00-\u9fa5])\s+([a-zA-Z])", r"\1\2", text)
    # 2. English before Chinese
    text = re.sub(r"([a-zA-Z])\s+([\u4e00-\u9fa5])", r"\1\2", text)
    return text


def replace_escape_sequences(text):
    # 這裡列出了一些常見的轉譯序列，可以根據需要進一步擴充
    escape_sequences = ["\n", "\t", "\r", "\u000b", "\\", "'", '"']
    for seq in escape_sequences:
        text = text.replace(seq, " ")
    return text


def load_ppt(file_path):
    prs = Presentation(file_path)
    result = {}
    for index, slide in enumerate(prs.slides):
        titles = []
        slide_content = {"標題": "", "內文": []}
        for shape in slide.shapes:
            if hasattr(shape, "text") and shape.text.strip() != "":
                # 將段落分隔符轉換為換行
                modified_text = "\n".join(
                    [para.text for para in shape.text_frame.paragraphs]
                )
                font_sizes = []
                # Get all font sizes from the text shape
                for paragraph in shape.text_frame.paragraphs:
                    for run in paragraph.runs:
                        if run.font.size:
                            font_sizes.append(run.font.size.pt)
                min_font_size = min(font_sizes) if font_sizes else None
                # Check for title
                if (shape.top <= 850000) or (
                    min_font_size is not None and min_font_size >= 40
                ):
                    titles.append(modified_text)
                # Check for content
                elif shape.top > 850000 and shape.top <= 6400000:
                    slide_content["內文"].append(
                        {
                            "種類": "內文",
                            "內容": modified_text,
                            "離PPT頁面左邊距離": int(shape.left),  # 使用int進行轉換
                            "離PPT頁面上邊距離": int(shape.top),  # 使用int進行轉換
                            "最小字體大小": min_font_size,
                        }
                    )
            # Check for table
            elif hasattr(shape, "table"):
                table_content = []
                text_table = ""  # 這裡用來儲存純文本表格
                for row in shape.table.rows:
                    # 轉換成純文本表格的一行
                    row_text = " | ".join(
                        [
                            "\n".join(
                                [para.text for para in cell.text_frame.paragraphs]
                            )
                            for cell in row.cells
                        ]
                    )
                    text_table += row_text + "\n"
                    text_table += "-" * len(row_text) + "\n"  # 分隔線
                    # 將段落分隔符轉換為換行
                    table_row = [
                        "\n".join([para.text for para in cell.text_frame.paragraphs])
                        for cell in row.cells
                    ]
                    table_content.append(table_row)
                slide_content["內文"].append(
                    {
                        "種類": "表格",
                        "內容": text_table.strip(),  # 使用純文本表格
                        "離PPT頁面左邊距離": int(shape.left),
                        "離PPT頁面上邊距離": int(shape.top),
                    }
                )
        # Set title
        if titles:
            slide_content["標題"] = "，".join(titles)
        else:
            slide_content["標題"] = "未有標題"
        result[f"第{index + 1}頁"] = slide_content  # 頁碼從1開始

    # 轉換為YAML格式
    return yaml.dump(result, allow_unicode=True)


def upload_file():
    filepath = filedialog.askopenfilename(
        title="選擇PPT檔案", filetypes=[("PPT files", "*.pptx")]
    )
    if not filepath:
        return
    content_dict = load_ppt(filepath)
    text_widgets[0].delete(1.0, tk.END)
    text_widgets[0].insert(tk.END, content_dict)
    # display_answer(content_dict)


######################################################################## PPT相關function
def main():
    """創建 tkinter 的主視窗、上傳按鈕和兩個文字顯示區域。"""
    root = tk.Tk()
    root.title("PPT 文字提取器")
    root.geometry("1200x450")
    # 定義一個新字體，大小比預設大2px
    default_font = font.nametofont("TkDefaultFont")
    larger_font = font.Font(font=default_font)
    larger_font.configure(size=default_font.cget("size") + 2)

    # Upload button
    upload_button = tk.Button(root, text="上傳PPT檔案", command=upload_file)
    upload_button.grid(row=0, column=0, columnspan=2, pady=10)

    # 新增提示文字和輸入框
    prompt_label = tk.Label(root, text="預設提示詞（可以修改）")
    prompt_label.grid(row=1, column=0, columnspan=2, pady=5)

    prompt_input = tk.Text(root, height=2, wrap=tk.WORD, font=larger_font)
    prompt_input.insert(tk.END, base_prompt)  # 預設值為base_prompt
    prompt_input.grid(row=2, column=0, columnspan=2, pady=5, sticky="ew")

    global text_widgets
    text_widgets = []
    for idx, (title, side) in enumerate(
        [("原始PPT內文", tk.LEFT), ("Azure OpenAI GPT-4 校稿結果", tk.RIGHT)]
    ):
        frame = Frame(root)
        label = Label(frame, text=title)
        label.pack(pady=10)
        text_widget = Text(frame, wrap=tk.WORD, font=larger_font)
        text_widget.pack(expand=True, fill=tk.BOTH)
        frame.grid(row=3, column=idx, sticky="nsew")  # 修改row為3
        text_widgets.append(text_widget)

    root.grid_rowconfigure(3, weight=1)  # 修改row為3
    root.grid_columnconfigure(0, weight=1)
    root.grid_columnconfigure(1, weight=1)
    root.mainloop()


if __name__ == "__main__":
    main()
