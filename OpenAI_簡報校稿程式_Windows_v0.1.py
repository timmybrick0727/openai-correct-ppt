import openai
import time
import re
import yaml
import tkinter as tk
import platform
from tkinter import filedialog, Text, Frame, Label, simpledialog, messagebox
from pptx import Presentation
from tkinter import font
from collections import OrderedDict


######################################################################## OpenAI基本設定
openai.api_type = "azure"
openai.api_base = "https://eademogpt4.openai.azure.com/"
openai.api_version = "2023-05-15"
openai.api_key = "fefb546daf384d20a82c5e52e41a40f3"
engine = "EA_GPT4_32K"
temperature = 0.5
max_tokens = 4000
frequency_penalty = 0.5
timeout = 10
sleep_time = 2
base_prompt = "以下YAML為一個pptx的內容，參考離PPT頁面左邊、上邊距離，對「內容」進行（1.拼寫檢查 2.語法檢查 3.標點符號必須為全形 4.檢查是否出現中文簡體、英文字詞全部大寫 5.檢查所有頁面所有文字及表格）；請用繁體中文條列需要修正之處「哪一頁：建議是什麼？修改前、修改後成什麼樣子？」"


######################################################################## OpenAI相關function
def clean_text(text):
    text = re.sub("<.*?>", "", text)
    text = re.sub("&nbsp;", " ", text)
    text = re.sub("&amp;", "&", text)
    text = re.sub("&gt;", ">", text)
    text = re.sub("&lt;", "<", text)
    return text


def get_answer(messages):
    result = clean_text(
        openai.ChatCompletion.create(
            engine=engine,
            messages=messages,
            temperature=temperature,
            max_tokens=max_tokens,
            top_p=0.95,
            frequency_penalty=frequency_penalty,
            presence_penalty=0,
            stop=None,
        )["choices"][0]["message"]["content"]
    )
    time.sleep(sleep_time)
    return result


loading_window = None
loading_label = None


def show_loading():
    """顯示「正在載入」的視窗。"""
    global loading_window, loading_label

    if not loading_window:
        loading_window = tk.Toplevel()
        loading_window.title("請稍候")
        loading_window.geometry("200x100")
        loading_label = tk.Label(loading_window, text="正在載入...")
        loading_label.pack(expand=True)
        # 使主視窗無法操作
        loading_window.grab_set()
        loading_window.update()


def hide_loading():
    """隱藏「正在載入」的視窗。"""
    global loading_window

    if loading_window:
        loading_window.grab_release()
        loading_window.destroy()
        loading_window = None


def refresh_display():
    show_loading()  # 顯示正在載入的視窗

    content_dict = text_widgets[0].get("1.0", tk.END).strip()
    if content_dict:
        display_answer(content_dict)

    hide_loading()  # 隱藏正在載入的視窗


def display_answer(data):
    show_loading()  # 顯示正在載入的視窗

    # 從 prompt_input 中取得最新的 base_prompt
    base_prompt = prompt_input.get("1.0", tk.END).strip()
    content = get_answer(
        [
            {
                "role": "user",
                "content": f"{base_prompt}\n\n{data}",
            }
        ]
    )
    text_widgets[1].delete(1.0, tk.END)
    text_widgets[1].insert(tk.END, content)

    hide_loading()  # 隱藏正在載入的視窗


######################################################################## PPT相關function
def custom_represent_string(dumper, data):
    if "\n" in data:
        return dumper.represent_scalar("tag:yaml.org,2002:str", data, style="|")
    return dumper.represent_scalar("tag:yaml.org,2002:str", data)


def custom_represent_float(dumper, data):
    return dumper.represent_scalar("tag:yaml.org,2002:float", str(data))


def custom_represent_int(dumper, data):
    return dumper.represent_scalar("tag:yaml.org,2002:int", str(data))


yaml.add_representer(str, custom_represent_string)
yaml.add_representer(float, custom_represent_float)
yaml.add_representer(int, custom_represent_int)


def ordereddict_to_dict(ordered_dict):
    # Convert OrderedDict to dict recursively
    return {
        k: ordereddict_to_dict(v) if isinstance(v, OrderedDict) else v
        for k, v in ordered_dict.items()
    }


def ordered_dict_representer(dumper, data):
    return dumper.represent_dict(data.items())


yaml.add_representer(OrderedDict, ordered_dict_representer)


def load_ppt(file_path):
    prs = Presentation(file_path)
    result = {}
    for index, slide in enumerate(prs.slides):
        titles = []
        slide_content = OrderedDict([("標題", ""), ("主文章", [])])
        for shape in slide.shapes:
            if hasattr(shape, "text") and shape.text.strip() != "":
                modified_text = "\n".join(
                    [para.text for para in shape.text_frame.paragraphs]
                )
                font_sizes = []
                for paragraph in shape.text_frame.paragraphs:
                    for run in paragraph.runs:
                        if run.font.size:
                            font_sizes.append(run.font.size.pt)
                min_font_size = min(font_sizes) if font_sizes else None
                if (shape.top <= 850000) or (
                    min_font_size is not None and min_font_size >= 40
                ):
                    titles.append(modified_text)
                elif shape.top > 850000 and shape.top <= 6400000:
                    content_dict = OrderedDict(
                        [
                            ("種類", "文字"),
                            ("內容", modified_text),
                        ]
                    )
                    slide_content["主文章"].append(content_dict)
            elif hasattr(shape, "table"):
                table_content = []
                for row in shape.table.rows:
                    table_row = [
                        "\n".join([para.text for para in cell.text_frame.paragraphs])
                        for cell in row.cells
                    ]
                    table_content.append(table_row)
                content_dict = OrderedDict(
                    [
                        ("種類", "表格"),
                        ("內容", table_content),
                    ]
                )
                slide_content["主文章"].append(content_dict)
        if titles:
            slide_content["標題"] = "，".join(titles)
        else:
            slide_content["標題"] = "未有標題"
        result[f"第{index + 1}頁"] = slide_content
    return yaml.dump(result, allow_unicode=True)


def upload_file():
    filepath = filedialog.askopenfilename(
        title="選擇PPT檔案", filetypes=[("PPT files", "*.pptx")]
    )
    if not filepath:
        return
    content_dict = load_ppt(filepath)
    text_widgets[0].delete(1.0, tk.END)
    text_widgets[0].insert(tk.END, content_dict)
    display_answer(content_dict)


######################################################################## UI相關function
def password_prompt() -> bool:
    """顯示密碼輸入視窗，檢查是否輸入正確的密碼，並返回True或False。"""
    password = simpledialog.askstring("密碼驗證", "請輸入密碼以繼續：", show="*")

    if password == "EA2023":
        return True
    else:
        messagebox.showerror("錯誤", "密碼錯誤，請重新輸入。")
        return False


def main():
    global text_widgets, prompt_input
    if not password_prompt():
        return
    """創建 tkinter 的主視窗、上傳按鈕和兩個文字顯示區域。"""
    root = tk.Tk()
    root.title("OpenAI 簡報校稿程式 v0.1")
    root.geometry("1200x450")
    # 定義一個新字體，大小比預設大2px
    default_font = font.nametofont("TkDefaultFont")
    larger_font = font.Font(font=default_font)
    larger_font.configure(size=default_font.cget("size") + 2)
    # Upload button
    upload_button = tk.Button(root, text="上傳PPT檔案", command=upload_file)
    upload_button.grid(row=0, column=0, columnspan=2, pady=10)
    # 新增"重整"按鈕
    refresh_button = tk.Button(root, text="重新詢問", command=refresh_display)
    refresh_button.grid(row=0, column=1, pady=10)
    # 新增提示文字和輸入框
    prompt_label = tk.Label(root, text="預設提示詞（可以修改）：", anchor="w")  # 在此行加入 anchor='w'
    prompt_label.grid(
        row=1, column=0, columnspan=2, pady=5, sticky="w"
    )  # 加入 sticky="w" 以確保標籤填滿整個空間的左側
    prompt_input = tk.Text(root, height=2, wrap=tk.WORD, font=larger_font)
    prompt_input.insert(tk.END, base_prompt)  # 預設值為base_prompt
    prompt_input.grid(row=2, column=0, columnspan=2, pady=5, sticky="ew")
    global text_widgets
    text_widgets = []
    for idx, title in enumerate(["原始PPT內文：", "Azure OpenAI GPT-4 校稿結果："]):
        frame = Frame(root)
        label = Label(frame, text=title, anchor="w")  # 在此行加入 anchor='w'
        label.pack(pady=10, fill="x")  # 為了讓label填滿整個frame的寬度,加入 fill='x'
        text_widget = Text(frame, wrap=tk.WORD, font=larger_font)
        text_widget.pack(expand=True, fill=tk.BOTH)
        frame.grid(row=3, column=idx, sticky="nsew")
        text_widgets.append(text_widget)
    root.grid_rowconfigure(3, weight=1)  # 修改row為3
    root.grid_columnconfigure(0, weight=1)
    root.grid_columnconfigure(1, weight=1)
    # 判斷作業系統
    os_name = platform.system()
    # 根據作業系統綁定複製快捷鍵
    if os_name == "Windows":
        root.bind("<Control-c>", lambda event=None: root.event_generate("<<Copy>>"))
    elif os_name == "Darwin":  # macOS的系統名稱是Darwin
        root.bind("<Command-c>", lambda event=None: root.event_generate("<<Copy>>"))
    root.mainloop()


if __name__ == "__main__":
    main()
